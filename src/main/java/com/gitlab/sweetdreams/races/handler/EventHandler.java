package com.gitlab.sweetdreams.races.handler;

import com.gitlab.sweetdreams.races.commands.CommandMain;
import com.gitlab.sweetdreams.races.race.RaceMain;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.animation.IEventHandler;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;

import java.util.Arrays;
import java.util.Collection;

@Mod.EventBusSubscriber
public class EventHandler implements IEventHandler
{
    @Override
    public void handleEvents(Object instance, float time, Iterable pastEvents)
    {

    }

    // To use when we have a GUI ready
    //@SubscribeEvent(priority = EventPriority.HIGHEST)
    //public void definePlayerRace(PlayerEvent.PlayerLoggedInEvent event)
    //{}

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void renderGameOverlay(RenderGameOverlayEvent event)
    {

        if(Minecraft.getInstance().player.isCreative() == false && event.getType() == RenderGameOverlayEvent.ElementType.ALL)
        {
            Minecraft minecraft = Minecraft.getInstance();

            int windowX = event.getWindow().getScaledWidth();
            int windowY = event.getWindow().getScaledHeight();

            RenderSystem.clearColor(1f, 1f, 1f, 1f);
            minecraft.getTextureManager().bindTexture(new ResourceLocation("races:textures/gui/test_cooldown_gui.png"));
            minecraft.ingameGUI.blit(event.getMatrixStack(), windowX / 2 + 10, windowY - 57, 0, 0, windowX / 2, windowY, 256, 256);
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public static void registerCommands(RegisterCommandsEvent event)
    {
        CommandDispatcher<CommandSource> commandDispatcher = event.getDispatcher();
        CommandMain.register(commandDispatcher);
    }
}
