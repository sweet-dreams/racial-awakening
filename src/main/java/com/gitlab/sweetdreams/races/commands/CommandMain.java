package com.gitlab.sweetdreams.races.commands;

import com.gitlab.sweetdreams.races.race.RaceMain;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;



public class CommandMain
{
    public static void register(CommandDispatcher<CommandSource> dispatcher)
    {
        LiteralArgumentBuilder<CommandSource> racialawakening = Commands.literal("racialawakening")
                .requires((commandSource) -> commandSource.hasPermissionLevel(2))
                    .then(Commands.literal("Air")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[0], 0)))
                    .then(Commands.literal("Earth")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[1], 1)))
                    .then(Commands.literal("Water")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[2], 2)))
                    .then(Commands.literal("Ice")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[3], 3)))
                    .then(Commands.literal("Nature")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[4], 4)))
                    .then(Commands.literal("Animal")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[5], 5)))
                    .then(Commands.literal("HellLord")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[6], 6)))
                    .then(Commands.literal("Wither")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[7], 7)))
                    .then(Commands.literal("Enderman")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[8], 8)))
                    .then(Commands.literal("Spirit")
                            .executes(commandContext -> RaceMain.setPlayerRace(commandContext.getSource().asPlayer(), RaceMain.racesList[9], 9)));



        dispatcher.register(racialawakening);
    }


}
